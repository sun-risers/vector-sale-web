export const PageConfig = {
  "pages": [
  {
    "label": "Handlers",
    "routerLink": "/dashboard",
    "icon": "fa fa-home",
    "priv": "DASHBOARD"
  },
  {
    "label": "#Tags",
    "routerLink": "",
    "icon": "fa fa-tags",
    "priv": "DASHBOARD"
  },
  {
    "label": "Post",
    "routerLink": "",
    "icon": "fa fa-home",
    "priv": "DASHBOARD"
  },
  {
    "label": "Place holders",
    "routerLink": "",
    "icon": "fa fa-home",
    "priv": "DASHBOARD"
  },
  {
    "label": "Users",
    "routerLink": "/users",
    "icon": "fa fa-users",
    "priv": "USERS",
  },{
    "label": "Roles",
    "routerLink": "/roles",
    "icon": "fa fa-user",
    "priv": "ROLES"
    }]
}

import {NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {NgbModule, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import 'rxjs/add/operator/toPromise';
// import { CookieService } from 'angular2-cookie/services/cookies.service';ssss
import { CookieService } from 'ngx-cookie-service';
// common
import {AppComponent} from './app.component';
import {AppTopBar} from './app.topbar.component';
import {AppFooter} from './app.footer.component';
// app common

import {  AuthGuard } from './shared/auth.service';
import { AuthService } from './services/auth.service';
import { PageService } from './shared/pages.service';
// import { ApiService } from './shared/api.serxvice';
import { RxWebsocketSubject } from './shared/ws.service';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { DashboardComponent } from './app-dashboard/app-dashboard';
import { ContributerLoginComponent, ContributerRegisterComponent } from './contributer/contributer.component';
import { ContactComponent } from './contact/contact.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { FaqComponent } from './faq/faq.component';
import { Home} from './home/home';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { SearchboxComponent } from './common/searchbox/searchbox.component';
import { TrailImageComponent } from './common/trail-image/trail-image.component';
import { UserPlansComponent } from './user-dashboard/user-plans/user-plans.component';
import {HeroService} from './app.topbar.service';
import { AccountComponent } from './user-dashboard/account/account.component';
import { PlansComponent } from './user-dashboard/account/plans/plans.component';
import { ProfileComponent } from './user-dashboard/account/profile/profile.component';
import { BillingComponent } from './user-dashboard/account/billing/billing.component';
import { PurchasehistoryComponent } from './user-dashboard/account/purchasehistory/purchasehistory.component';
import { PreferencesComponent } from './user-dashboard/account/preferences/preferences.component';
import { MyDashboardComponent } from './user-dashboard/my-dashboard/my-dashboard.component';
import { MyProfileComponent } from './user-dashboard/my-profile/my-profile.component';
import { MyInvoicesComponent } from './user-dashboard/my-invoices/my-invoices.component';
import { MyPurchasesComponent } from './user-dashboard/my-purchases/my-purchases.component';
import { MyVendorComponent } from './user-dashboard/my-vendor/my-vendor.component';
import { MyWishlistComponent } from './user-dashboard/my-wishlist/my-wishlist.component';
import { WishlistViewComponent } from './user-dashboard/my-wishlist/wishlist-view/wishlist-view.component';
import { WishlistEditComponent } from './user-dashboard/my-wishlist/wishlist-edit/wishlist-edit.component';


// app components
@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        NgbModule.forRoot()
    ],
    declarations: [
        AppComponent,
        AppTopBar,
        AppFooter,
        DashboardComponent,
        ContactComponent,
        AboutUsComponent,
        FaqComponent,
        LoginComponent,
        RegisterComponent,
        UserDashboardComponent,
        SearchboxComponent,
        TrailImageComponent,
        ContributerLoginComponent,
        ContributerRegisterComponent,
        UserPlansComponent,
        Home,
        AccountComponent,
        PlansComponent,
        ProfileComponent,
        BillingComponent,
        PurchasehistoryComponent,
        PreferencesComponent,
        MyDashboardComponent,
        MyProfileComponent,
        MyInvoicesComponent,
        MyPurchasesComponent,
        MyVendorComponent,
        MyWishlistComponent,
        WishlistViewComponent,
        WishlistEditComponent
        ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        AuthGuard,
        AuthService,
        PageService,
        NgbActiveModal,
        HeroService,
        CookieService
    ],
    entryComponents: [
        ContributerLoginComponent,
        ContributerRegisterComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

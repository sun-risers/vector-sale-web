import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes} from '@angular/router';
import { CommonModule } from '@angular/common';
import { ContributerComponent } from './contributer.component';
import { ContributerDashboardModule} from './contributer-dashboard/contributer-dashboard.module'
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
// export function loadContributerDashModule() {
//     return require('./contributer-dashboard/contributer-dashboard.module')('ContributerDashboardModule');
//   }
export const contributerRoutes: Routes  = [{
    path : '',
    component: ContributerComponent
}, {
    path : 'dashboard',
    loadChildren: './contributer-dashboard/contributer-dashboard.module#ContributerDashboardModule'//() => ContributerDashboardModule
}
];
@NgModule({
    imports : [
        CommonModule,
        FormsModule,
        NgbModule,
        RouterModule.forChild(contributerRoutes)
    ],
    declarations: [
        ContributerComponent
    ]
})
export class ContributerModule {}
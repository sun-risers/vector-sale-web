import { Component, OnInit } from '@angular/core';
import { categories } from '../../../shared/categoryList';
import {DialogModule} from 'primeng/primeng';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadModalComponent } from '../upload-modal/upload-modal.component';

@Component({
  selector: 'app-contributer-add-content',
  templateUrl: './contributer-add-content.component.html',
  styleUrls: ['./contributer-add-content.component.scss']
})
export class ContributerAddContentComponent implements OnInit {

  categoryList:any;
  hasUploadedImage:boolean=false;
  showModal:boolean=false;
  previewUrl:any;
  modalRef: any;
  tags:string[]=[];
  
  display: boolean = false;

  showDialog() {
      this.display = true;
  }
  
  constructor(private modalService: NgbModal) { }

  ngOnInit() {
    this.categoryList= categories;
  }
  openModal(){
    console.log("in method");
    this.modalRef = this.modalService.open(UploadModalComponent, { size: 'lg', backdrop: 'static' });
    this.modalRef.componentInstance.advertiser= 'addContent';
  }

  onTagsChanged(ev){
    console.log(ev);
    console.log(this.tags);
    
  }


  

}

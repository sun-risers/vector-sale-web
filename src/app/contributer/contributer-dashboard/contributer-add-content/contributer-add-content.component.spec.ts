import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributerAddContentComponent } from './contributer-add-content.component';

describe('ContributerAddContentComponent', () => {
  let component: ContributerAddContentComponent;
  let fixture: ComponentFixture<ContributerAddContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributerAddContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributerAddContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

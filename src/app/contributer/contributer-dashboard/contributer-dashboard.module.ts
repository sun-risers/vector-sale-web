import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes} from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ContributerDashboardComponent } from './contributer-dashboard.component';
import { HomeComponent } from './home/home.component';
import { ContributerAddContentComponent } from './contributer-add-content/contributer-add-content.component';
import { ContributerContentsComponent } from './contributer-contents/contributer-contents.component';
import { ContributerProfileComponent } from './contributer-profile/contributer-profile.component';
import { ContributerEarningsComponent } from './contributer-earnings/contributer-earnings.component';
import { UploadModalComponent } from './upload-modal/upload-modal.component';
import { TagsInputModule } from 'ngx-tags-input/dist';

export const contributerDashRoutes: Routes  = [{
    path : '',
    component: ContributerDashboardComponent
},{
    path : 'home',
    component: HomeComponent
},{
    path : 'profile',
    component: ContributerProfileComponent
},{
    path : 'contents',
    component: ContributerContentsComponent
},{
    path : 'addContent',
    component: ContributerAddContentComponent
},{
    path : 'earnings',
    component: ContributerEarningsComponent
},{
    path: 'uploads',
    component: UploadModalComponent
}
];
@NgModule({
    imports : [
        CommonModule,
        FormsModule,
        NgbModule,
        TagsInputModule.forRoot(),
        RouterModule.forChild(contributerDashRoutes)
    ],
    declarations: [
        HomeComponent,
        ContributerDashboardComponent,
        ContributerAddContentComponent,
        ContributerContentsComponent,
        ContributerProfileComponent,
        ContributerEarningsComponent,
        UploadModalComponent
    ],
    exports: [HomeComponent,RouterModule]
})
export class ContributerDashboardModule { }
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contributer-earnings',
  templateUrl: './contributer-earnings.component.html',
  styleUrls: ['./contributer-earnings.component.scss']
})
export class ContributerEarningsComponent implements OnInit {

  unpaidListdata=[
    {img:"../../assets/images/shiny-night-city_1127-8.jpg",item:"VS9407 – Abstract light blue violet background.",price:"$0", rate:'0%', date:'July 23, 2017'},
    {img:"../../assets/images/shiny-night-city_1127-8.jpg",item:"VS9407 – Abstract light blue violet background.",price:"$0", rate:'0%', date:'June 6, 2017'},
    {img:"../../assets/images/shiny-night-city_1127-8.jpg",item:"VS9407 – Abstract light blue violet background.",price:"$0", rate:'0%', date:'May 26, 2018'},
    {img:"../../assets/images/shiny-night-city_1127-8.jpg",item:"VS9407 – Abstract light blue violet background.",price:"$0", rate:'0%', date:'August 23, 2017'}
  ]

  paidListdata=[];

  revokedListdata=[];
  
  constructor() { }

  ngOnInit() {
  }

}

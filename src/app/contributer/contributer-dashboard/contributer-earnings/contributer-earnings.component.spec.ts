import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributerEarningsComponent } from './contributer-earnings.component';

describe('ContributerEarningsComponent', () => {
  let component: ContributerEarningsComponent;
  let fixture: ComponentFixture<ContributerEarningsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributerEarningsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributerEarningsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

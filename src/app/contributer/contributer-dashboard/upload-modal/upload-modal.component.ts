import { Component, OnInit, HostListener } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';


@Component({
  selector: 'app-upload-modal',
  templateUrl: './upload-modal.component.html',
  styleUrls: ['./upload-modal.component.scss']
})
export class UploadModalComponent implements OnInit {

  display: boolean = false;
  imgUrls:any;
  advertiser : any;
  names:string[]=[];
  
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.display=true;
    console.log('advertiser',this.advertiser);
    
  }

  close() {
    $('.modal-content').addClass('model-content-close');
    setTimeout(() => this.activeModal.close({ 'data': 'sucess' }), 10);
  }

  @HostListener('drop', ['$event']) public onDrop(evt){
    evt.preventDefault();
    evt.stopPropagation();
    let files = evt.dataTransfer.files;
    if(files.length > 0){
    }
  }

  onFileChange(ev){
    let files=ev.target.files;
    for(let file of files){
      this.createJSON(file,files);
    }
    
  }

  check(file,files){
    let fileName=file.name;
    let index=fileName.lastIndexOf('.');
    let name=fileName.slice(0, index);
    let extension=fileName.slice(index,fileName.length)
    for(let file of files){
      if(extension=='.jpg'&&file.name==(name+'.eps')){
        return true
      }
      if(extension=='.eps'&&file.name==(name+'.jpg')){
        return true
      }
    }
    return false
  }

  createJSON(file, files){
    if(this.check(file,files)){
      console.log('fileuploaded',file.name);
    }
  }

  onUploadID(ev){
    let file=ev.target.files[0];
  }

}

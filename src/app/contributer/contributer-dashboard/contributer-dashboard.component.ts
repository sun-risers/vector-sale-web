import { Component, OnInit } from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {HomeComponent } from './home/home.component';

@Component({
  selector: 'app-contributer-dashboard',
  templateUrl: './contributer-dashboard.component.html',
  styleUrls: ['./contributer-dashboard.component.css'],
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({opacity: 0}),
        animate('400ms ease-in-out', style({opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translate(0)'}),
        animate('400ms ease-in-out', style({opacity: 0}))
      ])
    ])
  ]
})
export class ContributerDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contributer-contents',
  templateUrl: './contributer-contents.component.html',
  styleUrls: ['./contributer-contents.component.scss']
})
export class ContributerContentsComponent implements OnInit {

  wishlistdata=[
    {img:"../../assets/images/shiny-night-city_1127-8.jpg",name:"Abstract light blue violet background",status:'LIVE',downloads:6,price:"$300",published_date:"16-04-2018", privs:['View','Edit', 'Delete']},
    {img:"../../assets/images/shiny-night-city_1127-8.jpg",name:"Abstract light blue violet background",status:'PENDING REVIEW',downloads:60,price:"$301", published_date:"06-04-2017", privs:['Edit', 'Delete']},
    {img:"../../assets/images/shiny-night-city_1127-8.jpg",name:"Abstract light blue violet background",status:'LIVE',downloads:16,price:"$302", published_date:"10-04-2018", privs:['View', 'Edit', 'Delete']},
    {img:"../../assets/images/shiny-night-city_1127-8.jpg",name:"Abstract light blue violet background",status:'LIVE',downloads:0,price:"$30", published_date:"16-01-2018", privs:['Edit','Delete']}
  ]
  
  constructor() { }

  ngOnInit() {
  }

  getCSSClasses(flag:string) {
    let cssClasses;
    if(flag == 'LIVE') {  
       cssClasses = {
         'live_style': true
       }	
    } else {  
       cssClasses = {
         'pending_color': true
       }	
    }
    return cssClasses;
  }

  getPrivClasses(flag:string){
    let cssClasses;
    if(flag == 'View') {  
       cssClasses = {
         'view_style': true,
         'm-r-5': true
       }	
    }
    if(flag == 'Edit') {  
      cssClasses = {
        'edit_style': true,
        'm-r-5': true
      }	
    }
    if(flag == 'Delete') {  
      cssClasses = {
        'delete_style': true,
        'm-r-5': true
      }	
    }

    return cssClasses;
  }

}

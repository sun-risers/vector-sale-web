import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributerContentsComponent } from './contributer-contents.component';

describe('ContributerContentsComponent', () => {
  let component: ContributerContentsComponent;
  let fixture: ComponentFixture<ContributerContentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributerContentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributerContentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributerProfileComponent } from './contributer-profile.component';

describe('ContributerProfileComponent', () => {
  let component: ContributerProfileComponent;
  let fixture: ComponentFixture<ContributerProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributerProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributerProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

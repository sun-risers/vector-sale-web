import { Component, OnInit } from '@angular/core';
import { countries } from "../../../shared/countriesList";
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadModalComponent } from '../upload-modal/upload-modal.component';

@Component({
  selector: 'app-contributer-profile',
  templateUrl: './contributer-profile.component.html',
  styleUrls: ['./contributer-profile.component.scss']
})
export class ContributerProfileComponent implements OnInit {

  countriesList:any;
  modalRef: any;
  
  constructor(private modalService: NgbModal) { }

  ngOnInit() {
    this.countriesList=countries
  }

  openModal(){
    console.log("in method");
    this.modalRef = this.modalService.open(UploadModalComponent, { size: 'lg', backdrop: 'static' });
    this.modalRef.componentInstance.advertiser= 'profile';
  }

}

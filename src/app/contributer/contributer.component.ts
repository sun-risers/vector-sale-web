import { Router } from '@angular/router';
import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'contributer-login',
    templateUrl: './login.component.html',
    styleUrls : ['./contributer.component.css'],
})

export class ContributerLoginComponent {
    @Input() isDisplay = true;
    @Output() isDisplayChange = new EventEmitter<boolean>();
    @Output() hideDisplay = new EventEmitter();
    constructor(public activeModal: NgbActiveModal,private router: Router) {}
    close() {
        this.hideDisplay.emit();
        this.isDisplayChange.emit(this.isDisplay);
        this.isDisplay = false;
    }
    navigateToContributerBoard() {
        this.activeModal.close({'data': 'sucess'});
        this.router.navigate(['/contributer/dashboard']);
    }
}

@Component({
    selector: 'contributer-register',
    templateUrl: './register.component.html',
    styleUrls : ['./contributer.component.css'],
})

export class ContributerRegisterComponent {

    @Input() isDisplay = true;
    @Output() isDisplayChange = new EventEmitter<boolean>();
    @Output() hideDisplay = new EventEmitter();
    constructor(public activeModal: NgbActiveModal) {}
    close() {
        this.hideDisplay.emit();
        this.isDisplayChange.emit(this.isDisplay);
        this.isDisplay = false;
    }
}

@Component({
    selector: 'contributer',
    templateUrl: './contributer.component.html',
    styleUrls : ['./contributer.component.css'],
})

export class ContributerComponent {
    isLogin = false;
    isRegister = false;
    modalRef: any;
    constructor(private modalService: NgbModal) {}
    // For login modal actions
    showLogin () {
        this.isLogin = true;
        this.modalRef = this.modalService.open(ContributerLoginComponent);
        this.modalRef.componentInstance.foo = 'test';
        // this.modalRef.componentInstance.out = this.contributerSignIn;
        this.modalRef.result.then((result) => {
            console.log('sucesss close login', result);
          }, (reason) => {
            console.log('dismiss consolee', reason);
          });
        // Use of  component from modalRef therej 2 methods
        // 1) modalRef = this.modalService.open(ContibuterLoginComponent).component;
        // then we can use directly modalRef.foo = "test"
        // 2)  modalRef = this.modalService.open(ContibuterLoginComponent)
        // modalRef.componentInstance.foo = "test"
    }


    // for register modal actions

    showRegister () {
        this.isRegister = true;
        const modalRef = this.modalService.open(ContributerRegisterComponent);
    }
    hideRegister() {
    }
}


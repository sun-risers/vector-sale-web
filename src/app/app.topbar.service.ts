import { Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class HeroService {
    constructor(private http: Http) {
    }

    private headerUrl = '/app/json-files/header.json'
    public getJSON(): Observable<any> {
         return this.http.get(this.headerUrl)	
                         .map((res:any) => res.json())
                          // .catch((err:any,caught:Observable<any>) => {ObservableInput<{}>;console.log(error)});

     }

}



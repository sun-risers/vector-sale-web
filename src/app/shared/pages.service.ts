import { Injectable } from '@angular/core';
import { PageConfig } from '../../assets/pages';

export { MenuItem } from './menuitem';

@Injectable()
export class PageService {
  userPages: any = []

  pages() {
    this.userPages = []
    let user = JSON.parse(localStorage.getItem("currentUser"));
    const privs = user.privileges;

    for (let page of PageConfig.pages) {
      let hasAccess = privs.filter(priv => priv.name === page.priv)[0];
      if (hasAccess && "submenu" in page) {
        this.userPages.push(this.computeSubmenu(page, privs))
        continue
      }
      if (hasAccess && !("submenu" in page)) {
        this.userPages.push(page)
      }
    }
    return this.userPages;
  }

  computeSubmenu(page: any, privs: any) {
    page.items = [];
    for (let subpage of page.submenu) {
      let hasAccess = privs.filter(priv => priv.name === subpage.priv)[0];
      if (hasAccess) {
        page.items.push(subpage);
      }
    }
    return page;
  }
}
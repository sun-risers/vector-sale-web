import { Injectable } from '@angular/core';
import { BaseRequestOptions, RequestOptions, URLSearchParams } from '@angular/http';
import { Http, Response, Headers } from '@angular/http';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { ApiService } from './api.service'
import { PageService } from '../shared/pages.service'


// Auth service
@Injectable()
export class AuthService {
  constructor(private http: Http, private apiService: ApiService, private router: Router, private pageService: PageService) {
  }

  isLoggedIn(): boolean {
    return this.currentUser() != null;
  }
  
  currentUser(): any {
    let user = localStorage.getItem('currentUser')
    if (user) return JSON.parse(user);
  }

  makeHeaders(username: string, password: string): Headers {
    let base64Credentials: string = window.btoa(username + ':' + password);
    let headers = new Headers()
    headers.append('Authorization', "Basic " + base64Credentials)
    return headers
  }

  login(username: string, password: string): Promise<any> {
    let data = new URLSearchParams();
    data.append('status', status);
    data.append('username', username);
    data.append('password', password);
    return this.apiService.post(`auth/myaccount/login`, data, this.makeHeaders(username, password))
      .then(response => {
        localStorage.setItem('currentUser', JSON.stringify(response));
      }).catch(err => {
        return Promise.reject(err);
        // this.isLoaded = true
      })
  }

  reset(data: Object): Promise<any> {
    return this.apiService.put(`auth/myaccount/password/reset`, data)
      .then(response => {
        localStorage.setItem('currentUser', JSON.stringify(response));
      }).catch(err => {
        return Promise.reject(err);
        // this.isLoaded = true
      })
  }

   myProfile(): Promise<any> {
    return this.apiService.get(`auth/myprofile`)
      .then(response => response)
  }


  updateMyProfile(data: Object): Promise<any> {
    return this.apiService.put(`auth/myprofile`, data)
      .then(response => {
        //localStorage.setItem('currentUser', JSON.stringify(response));
      }).catch(err => {
        return Promise.reject(err);
        // this.isLoaded = true
      })
  }

  updatePassword(data: Object): Promise<any> {
    return this.apiService.put(`auth/myaccount/password/update`, data)
      .then(response => response)
  }

  logout(): void {
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}

// To protect angular routes
@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService) { }

  canActivate() {
    if (!this.authService.isLoggedIn()) {
      this.authService.logout()
      // return false
    }
    
    return true
  }
}
import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import { environment } from '../../environments/environment';

const hostName = window.location.hostname

const BASE_API_URL = environment[hostName].apiUrl
const BASE_WS_URL = environment[hostName].wsUrl;
const COMPANY_ID = environment.companyID;

export class Pager {
  constructor() { }
  public total: number; //total records
  public page: number; // current page
  public size: number; // page size
}

export class PagedRecords {
  public status: string;
  public total: number;
  public totalRecords: number;
  public records: number;
  public rows: Object[];
}

@Injectable()
export class ApiService {
  constructor(private http: Http, private router: Router) { 
  }


  getBaseAPIUrl():string{
    if (hostName == "localhost"){
      return BASE_API_URL
    }
    return "api/"
  }

  get(path: string, params?: URLSearchParams | any): Promise<any> {
    let fullUrl = this.getBaseAPIUrl() + path;
    let options = { 'headers': this.makeHeaders() }
    if (params instanceof URLSearchParams) {
      options['search'] = params
    } else if (params instanceof String) {
      fullUrl += '?' + params
    }
    return this.http.get(fullUrl, options)
      .toPromise()
      .then(this.handleResponse.bind(this))
      .catch(this.handleError.bind(this));
  }

  getQueryParams(filter: Object,page: number, size: number){
    let query = new URLSearchParams();
    query.set('page', page.toString())
    query.set('limit', size.toString())

    if (Object.keys(filter).length != 0) {
      for (let key of Object.keys(filter)) {
        query.set(key, filter[key])  
        if(typeof(filter[key]) == "object"){
          query.set(key,JSON.stringify(filter[key]));
        }
      }
    }
    return query;
  }

  getWithFilter(path :string,filter: Object, page: number, size: number): Promise<any> {
    let fullUrl = this.getBaseAPIUrl() + path;
    let options = { 'headers': this.makeHeaders() }
    let params:any = this.getQueryParams(filter,page,size)
    if (params instanceof URLSearchParams) {
      options['search'] = params
    } else if (params instanceof String) {
      fullUrl += '?' + params
    }
    return this.http.get(fullUrl, options)
      .toPromise()
      .then(this.handleResponse.bind(this))
      .catch(this.handleError.bind(this));
  }

  post(path: string, body: URLSearchParams | any = {}, headers: Headers | any = {}): Promise<any> {
    let fullUrl = this.getBaseAPIUrl() + path;
    return this.http.post(fullUrl, body, { headers: this.makeHeaders(headers) })
      .toPromise()
      .then(this.handleResponse.bind(this))
      .catch(this.handleError.bind(this));
  }

  put(path: string, body: URLSearchParams | any = {}): Promise<any> {
    let fullUrl = this.getBaseAPIUrl() + path;
    return this.http.put(fullUrl, body, { headers: this.makeHeaders() })
      .toPromise()
      .then(this.handleResponse.bind(this))
      .catch(this.handleError.bind(this));
  }

  delete(path: string): Promise<any> {
    let fullUrl = this.getBaseAPIUrl() + path;
    return this.http.delete(fullUrl, { headers: this.makeHeaders() })
      .toPromise()
      .then(this.handleResponse.bind(this))
      .catch(this.handleError.bind(this));
  }

  private handleResponse(resp: Response): Promise<any> {
    let msg = resp.json()
    if (msg.success == false) {
      return Promise.reject(resp);
    }

    return Promise.resolve(msg);
  }

  private handleError(resp: Response | any): Promise<any> {
    if (resp.status == 200) {
      // this.alertService.error(resp.json().message)
      return Promise.reject(resp.json());
    }

    if (resp.status == 400) {
      let errMsg = resp.statusText || 'Bad request'
      // this.alertService.error(errMsg)
      return Promise.reject(resp);
    }

    if (resp.status == 401) {
      this.logout()
      return Promise.reject(resp)
    }

    if (resp.status == 403) {
      let errMsg = resp.statusText || 'Invalid session'
      // this.alertService.error(errMsg)
      return Promise.reject(resp)
    }

    if (resp.status == 500) {
      let errMsg = resp.statusText || 'Server error'
      // this.alertService.error(errMsg)
      return Promise.reject(resp);
    }

    if (resp.status == 0) {
      // this.alertService.error('Something wrong. Failed to connect to the api server.')
      return Promise.reject(resp);
    }
  }


  private makeHeaders(customHeaders: Headers | any = {}) {
    let headers = new Headers();
    if (Object.keys(customHeaders).length != 0) {
      for (let key of customHeaders.keys()) {
        headers.set(key, customHeaders.get(key))
      }
    }

    headers.append('companyID', COMPANY_ID)
    let currentUser = localStorage.getItem('currentUser')
    if (currentUser) {
      let user = JSON.parse(currentUser)
      headers.append('Authorization', 'Token ' + user['token']);
    }

    return headers;
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  getCurrentPage(event: any): number {
    let currentPage: number;
    currentPage = event.first / event.rows + 1;
    return currentPage
  }

}

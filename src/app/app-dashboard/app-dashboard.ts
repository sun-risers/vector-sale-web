import { Component } from '@angular/core';
import {trigger,state,style,transition,animate,keyframes,query,stagger}  from '@angular/animations';

@Component({
  selector: 'app-dashboard',
  templateUrl: './app.dashboard.html',
  styleUrls: ['./app.dashboard.css'],
  animations: [
    trigger('bannerAnimations', [
      transition('* => *', [
          query('.second-child',style({opacity:0,transform: 'translateY(75%)'})),
        query('.first-child', stagger('300ms', [
          animate('800ms ease-in', keyframes([
            style({opacity: 0, transform: 'translateY(-75%)', offset: 0}),
            style({opacity: 1, transform: 'translateY(0)',     offset: 1.0}),
          ]))]),{optional:true}),
          query('.second-child', stagger('300ms', [
          animate('1s ease-in', keyframes([            
              style({opacity: 0.3, transform: 'translateY(35%)',     offset: 0.3}),
            style({opacity: 1, transform: 'translateY(0)',     offset: 1.0}),
          ]))]),{optional:true})        

      ])
    ])
  ]
})
export class DashboardComponent { }
import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {
  Router,
  NavigationExtras
} from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as $ from 'jquery';
import { AuthService } from '../services/auth.service';
import * as _ from 'underscore';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginForm: FormGroup;

  constructor(public activeModal: NgbActiveModal, private router: Router, private authService: AuthService,private fb: FormBuilder) 
  {  
    this.createForm();
  }
  createForm(){
    this.loginForm = this.fb.group({
      userName: '', // <--- the FormControl called "name"
      password:''
    });
  }

  onSignIn() {
    if(_.isEmpty(this.loginForm) && _.isEmpty(this.loginForm.controls)){
      return
    }
    let credentials = {
      "username":this.loginForm.controls.userName['value'],
      "password":this.loginForm.controls.password['value'],
    }
    
    this.authService.authenticateUser(credentials);
    this.close()
    this.router.navigate(['/userdashboard']);
  }
  close() {
    $('.modal-content').addClass('model-content-close');
    setTimeout(() => this.activeModal.close({ 'data': 'sucess' }), 10);
  }

}

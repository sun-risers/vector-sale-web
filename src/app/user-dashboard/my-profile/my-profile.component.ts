import { Component, OnInit } from '@angular/core';
import { countries } from "../../shared/countriesList";

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {

  countriesList:any;
  
  constructor() { }

  ngOnInit() {
    this.countriesList=countries;
  }

}

import { Component, OnInit } from '@angular/core';
import {SearchboxComponent } from '../common/searchbox/searchbox.component';
import {TrailImageComponent } from '../common/trail-image/trail-image.component';
import { UserPlansComponent } from './user-plans/user-plans.component';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {


  constructor() { }

  ngOnInit() {
  }

}

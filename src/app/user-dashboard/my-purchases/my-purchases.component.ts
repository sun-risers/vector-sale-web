import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-purchases',
  templateUrl: './my-purchases.component.html',
  styleUrls: ['./my-purchases.component.scss']
})
export class MyPurchasesComponent implements OnInit {

  downloadhistory=[
    {img:"../../assets/images/shiny-night-city_1127-8.jpg",name:"downtown image",comment:"No downloadable files found."}
  ]
  
  constructor() { }

  ngOnInit() {
  }

}

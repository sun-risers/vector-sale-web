import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-wishlist-view',
  templateUrl: './wishlist-view.component.html',
  styleUrls: ['./wishlist-view.component.scss']
})
export class WishlistViewComponent implements OnInit {

  editView:boolean;
  @Input() listName:string;

  wishlistdata=[
    {img:"../../assets/images/shiny-night-city_1127-8.jpg",item:"VS9407 – Abstract light blue violet background.",price:"$300"},
    {img:"../../assets/images/shiny-night-city_1127-8.jpg",item:"VS9407 – Abstract light blue violet background.",price:"$301"},
    {img:"../../assets/images/shiny-night-city_1127-8.jpg",item:"VS9407 – Abstract light blue violet background.",price:"$302"},
    {img:"../../assets/images/shiny-night-city_1127-8.jpg",item:"VS9407 – Abstract light blue violet background.",price:"$30"}
  ]

  constructor() { }

  ngOnInit() {
  }

  onRemove(i){
    var index = this.wishlistdata.indexOf(i, 0);
    if (index > -1) {
      this.wishlistdata.splice(index, 1);
    }
  }

  onEditSettings(){
    this.editView=true;
  }

}

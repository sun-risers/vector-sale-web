import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-wishlist',
  templateUrl: './my-wishlist.component.html',
  styleUrls: ['./my-wishlist.component.scss']
})
export class MyWishlistComponent implements OnInit {

  value:boolean;
  editvalue:boolean;
  sampleArt:string;
  
  constructor() { }

  ngOnInit() {
  }

  onWishClick(){
    this.value=true;
    this.sampleArt="sampleArt";
  }

  onEditClick(){
    this.editvalue=true;
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-dashboard',
  templateUrl: './my-dashboard.component.html',
  styleUrls: ['./my-dashboard.component.scss']
})
export class MyDashboardComponent implements OnInit {
  subscriptiondetails =[
    {status:"pending",subscription:"Platinum",Expiration:"	April 5, 2018",actions:["Renew your subscription","Upgrade or change your subscription"]}
  ]

  invoicedetails=[
    // {invoice:"#123",subscription:"Platinum",amount:"$120",paymentstatus:"Unpaid",date:"April 2, 2018",actions:""}
  ]
  constructor() { }

  ngOnInit() {
  }

}

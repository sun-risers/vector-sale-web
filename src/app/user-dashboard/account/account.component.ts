import { Component, OnInit } from '@angular/core';
const list = require('../../json-files/account-list.json');
import * as _ from 'underscore';
import { Router } from '@angular/router';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  public accountList;
  constructor(private router: Router) {}

  ngOnInit() {
    this.accountList = JSON.parse(JSON.stringify(list));
    this.selectedState()
  }

  selectedState() {
    _.each(this.accountList.account, (item) => {
      item.isActive =  (this.getLink(item) == this.router.url) ?true:false;
    })
  }
  getLink(item) {
    let link = "/account/" + item.state;
    return link;
  } 

  onClick(selected) {
    _.each(this.accountList.account, (item) => {
      item.isActive =  (item.lableName === selected.lableName) ?true:false;
    })
  }
}

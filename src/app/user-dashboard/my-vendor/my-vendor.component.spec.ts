import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyVendorComponent } from './my-vendor.component';

describe('MyVendorComponent', () => {
  let component: MyVendorComponent;
  let fixture: ComponentFixture<MyVendorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyVendorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyVendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

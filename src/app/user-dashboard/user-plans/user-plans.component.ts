import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-plans',
  templateUrl: './user-plans.component.html',
  styleUrls: ['./user-plans.component.css']
})
export class UserPlansComponent implements OnInit {

  myelement:any;
  constructor() { }

  ngOnInit() {
    this.myelement="dashboard";
  }

  onClick(elements){
    this.myelement=elements;
  }

}

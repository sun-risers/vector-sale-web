import { Injectable } from '@angular/core';
const users = require('./../json-files/users.json');
import * as _ from 'underscore';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthService {
  usersList
  constructor(private cookieService:CookieService) { 
    this.usersList = JSON.parse(JSON.stringify(users));
  }
  authenticateUser(credentials){
   const user =  _.findWhere(this.usersList,{"username":credentials.username });
   if(!_.isUndefined(user) && !_.isEmpty(user)){
    this.cookieService.set( 'user', user );
   }else{

  }
   
  }

}

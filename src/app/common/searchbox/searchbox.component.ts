import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-searchbox',
  templateUrl: './searchbox.component.html',
  styleUrls: ['./searchbox.component.css']
})
export class SearchboxComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  	this.selectDropDownValue();
  }
  selectDropDownValue(){
  	$(".search-dropdown-menu a").click(function(){
  		var selText = $(this).text();
  		$(this).parents('.input-group-append').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
	});
  }

}

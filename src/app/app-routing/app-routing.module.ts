import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from '../app-dashboard/app-dashboard';
import { ContactComponent } from '../contact/contact.component';
import { AboutUsComponent } from '../about-us/about-us.component';
import { FaqComponent } from '../faq/faq.component';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { UserDashboardComponent } from '../user-dashboard/user-dashboard.component';
import { AccountComponent } from '../user-dashboard/account/account.component';
import { PlansComponent } from '../user-dashboard/account/plans/plans.component';
import { ProfileComponent } from '../user-dashboard/account/profile/profile.component';
import { BillingComponent } from '../user-dashboard/account/billing/billing.component';
import { PurchasehistoryComponent } from '../user-dashboard/account/purchasehistory/purchasehistory.component';
import { PreferencesComponent } from '../user-dashboard/account/preferences/preferences.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'aboutus', component: AboutUsComponent },
  { path: 'faq', component: FaqComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: RegisterComponent },
  {
    path: 'contributer',
    loadChildren: '../contributer/contributer.module#ContributerModule'
  },
  { path: 'userdashboard', component: UserDashboardComponent },
  {
    path: 'account',
    component: AccountComponent,
    children: [
      {
        path: 'plans',
        component: PlansComponent
      }, {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: 'billing',
        component: BillingComponent
      },
      {
        path: 'purchasehistory',
        component: PurchasehistoryComponent
      },{
        path: 'preferences',
        component: PreferencesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
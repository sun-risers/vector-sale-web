import { Component, Inject, forwardRef, Injector, OnInit } from '@angular/core';
import { AppComponent } from './app.component';
import { ApiService } from './shared/api.service';
import { Router } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HeroService } from './app.topbar.service';
import * as _ from 'underscore';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-topbar',
  templateUrl: './app.topbar.component.html'
})
export class AppTopBar implements OnInit {
  modalRef: any;

  constructor(private modalService: NgbModal, private headerService: HeroService, private cookieService: CookieService) {

  }
  private menu: any;


  ngOnInit() {
    this.menu = {};
    this.loadTabFields()
  }

  eval(item) {
    if (_.isUndefined(item.isPresent)) {
      return false;
    }
    return this[item.isPresent](item);
  }

  evalOnClick(item) {
    if (_.isUndefined(item.action)) {
      return;
    }
    this[item.action](item);
  }

  showLogin = function (item) {
    this.modalRef = this.modalService.open(LoginComponent);
    this.modalRef.result.then((result) => {
      console.log('sucesss close login', result);
    }, (reason) => {
      console.log('dismiss consolee', reason);
    });
  }

  showRegister = function () {
    this.modalRef = this.modalService.open(RegisterComponent);
    this.modalRef.result.then((result) => {
      console.log('sucesss close register', result);
    }, (reason) => {
      console.log('dismiss console', reason);
    });
  }

  checkForMyAccount() {
    let isHidden = true;
    isHidden = (this.cookieService.get('user')) ? false : true;
    return isHidden;
  }

  checkForLogin() {
    let isHidden = false,user =this.cookieService.get('user');
    
    isHidden = ( user && !_.isUndefined(user)) ? true : false;
    return isHidden;
  }

  checkForRegister() {
    return this.checkForLogin();
  }

  loadTabFields() {
    this.headerService.getJSON().subscribe(data => {
      this.menu = data;
    }, error => console.log(error));
  }

  navigateToSubscribe() {
    console.log("clicked on pricing ")
  }

  signOut() {
    this.cookieService.delete('user');
  }
}

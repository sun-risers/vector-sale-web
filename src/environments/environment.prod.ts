export const environment = {
  production: true,
  companyID: 'VECTOR',

  "localhost" : {
    apiUrl: 'https://dev.sunriser.com/',
    wsUrl: 'ws://localhost:5000/'
  }
};

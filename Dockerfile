FROM mhart/alpine-node:9

ADD . /src
WORKDIR /src

RUN npm install -g @angular/cli http-server \
    && npm install \
    && ng build \
    && mv /src/dist /app \
    && cd /app \
    && rm -rf /src \
    && npm uninstall -g @angular/cli \
    && npm cache clean

WORKDIR /app
EXPOSE 4200

CMD ["http-server"]
